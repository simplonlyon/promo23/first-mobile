import { useEffect, useState } from "react";
import { NativeScrollEvent, NativeSyntheticEvent, RefreshControl, ScrollView, Text } from "react-native";
import { Picture } from "../entities";
import { fetchAllPictures } from "../picture-service";
import PictureListItem from "../components/PictureListItem";


export default function PictureScreen() {
    const [pictures, setPictures] = useState<Picture[]>([]);
    const [page,setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [maxPage, setMaxPage] = useState(10)
    
    useEffect(() => {
        fetchAllPictures(page)
            .then(pagePicture => {
                setMaxPage(pagePicture.pageTotal);
                setPictures([...pictures, ...pagePicture.data])
                setLoading(false);
            }).catch(err => console.log(err));
    }, [page]);

    function loadNext(event:NativeSyntheticEvent<NativeScrollEvent>) {
        // setPage(page+1);
        if(event.nativeEvent.contentOffset.y >= event.nativeEvent.contentSize.height - 750 && !loading && page < maxPage) {
            setLoading(true);
            setPage(page+1);
            console.log('bottom');
        }
    }

    function refresh() {
        setPictures([]);
        setPage(1);
    }

    return (
        <ScrollView onScroll={loadNext} refreshControl={<RefreshControl refreshing={loading} onRefresh={refresh} />}>
            {pictures.map(item => <PictureListItem key={item.id} picture={item} />)}
            {page == maxPage && <Text>No more content, go touch grass</Text>}
            {/* <Button title="Next" onPress={loadNext}/> */}
        </ScrollView>
    )
}