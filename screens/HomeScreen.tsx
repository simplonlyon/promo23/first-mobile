import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, View } from 'react-native';
import Counter from '../components/Counter';
import { useNavigation } from '@react-navigation/native';

export default function HomeScreen() {
  const navigation = useNavigation<any>();

  return (
    <View style={styles.container}>
      <Counter />
      <Button title="Camera" onPress={() => navigation.navigate("Camera")} />
      <Button title="Picture" onPress={() => navigation.navigate("Picture")} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
