import { Button, Image, Text, View } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { DeviceMotion, DeviceMotionMeasurement } from 'expo-sensors'
import { useEffect, useState } from "react";

export default function CameraScreen() {
    const [picked, setPicked] = useState('');
    const [position, setPosition] = useState({
        left: 0,
        top: 0
    });
    const [data, setData] = useState<DeviceMotionMeasurement>();
    async function pickImage(camera = true) {
        let result;
        if (camera) {
            result = await ImagePicker.launchCameraAsync();

        } else {
            result = await ImagePicker.launchImageLibraryAsync();
        }
        if (result.assets?.length) {
            setPicked(result.assets[0].uri);
        }
    }

    useEffect(() => {
        let subscribtion:any;
        //On peut faire tout ça avec le Accelerometer aussi, on obtient les mêmes infos
        async function startSensor() {
            await DeviceMotion.requestPermissionsAsync();
            DeviceMotion.setUpdateInterval(50);
            subscribtion = DeviceMotion.addListener((data) => {
                setPosition(current => ({
                    left: current.left+(data.rotation.gamma*10),
                    top: current.top+(data.rotation.beta*10),
                }));
            });
        }
        startSensor();

        return subscribtion?.remove();

    }, [])


    return (
        <View style={{ flex: 1 }}>
            <Text>{data?.rotation?.beta} {data?.rotation?.gamma} {data?.rotation?.alpha}</Text>
            <Button title="take picture" onPress={() => pickImage()} />
            <Button title="choose picture" onPress={() => pickImage(false)} />
            {picked &&
                <Image source={{ uri: picked }} style={{ width: 100, height: 200, position:'absolute', left: position.left, top: position.top }} />
            }
        </View>
    )
}