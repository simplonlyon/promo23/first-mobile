import axios from "axios";
import { Picture, PicturePage } from "./entities";


export async function fetchAllPictures(page = 1) {
    const response = await axios.get<PicturePage>(`http://10.0.10.174:8000/api/picture?page=${page}&pageSize=5`);
    return response.data;
}


export async function postPicture(picture: Picture) {
    const response = await axios.post<Picture>('http://10.0.10.174:8000/api/picture', picture);
    return response.data;
}



export async function likePicture(picture: Picture) {
    const response = await axios.patch<Picture>('http://10.0.10.174:8000/api/picture/'+picture.id+'/like');
    return response.data;
}
