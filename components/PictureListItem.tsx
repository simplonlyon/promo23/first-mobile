import { Button, Image, StyleSheet, Text, View } from "react-native";
import { Picture } from "../entities";

interface Props {
    picture: Picture;
}

export default function PictureListItem({ picture }: Props) {

    return (
        <View style={styles.container}>
            <Image source={{uri: picture.src}} style={styles.img} />
            <Text>{picture.description}</Text>
            <Button title={"Like ("+picture.likes?.length+")"} />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '95%',
        shadowOpacity: 0.5,
        shadowRadius: 10,
        borderWidth: 1,
        padding: 10,
        margin: 10
    },
    img: {
        width: '100%',
        height: 400
    }
  });
  