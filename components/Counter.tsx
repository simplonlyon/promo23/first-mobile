import { useState } from "react";
import { Button, Text } from "react-native";


export default function Counter() {

    const [count, setCount] = useState(0);

    function increment() {
        setCount(count + 1);
    }
    return (
        <>
            <Text>Count value : {count}</Text>
            <Button title="Count" onPress={increment}></Button>
        </>
    )
}